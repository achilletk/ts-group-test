package ts.group.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudAppliApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudAppliApplication.class, args);
	}

}
