package ts.group.crud.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import ts.group.crud.repository.ProductRepository;
import org.springframework.stereotype.Service;
import ts.group.crud.model.Product;

import lombok.Data;

@Data
@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Optional<Product> getProductById(final Long id) {
        return productRepository.findById(id);
    }

    public Iterable<Product> getProducts() {
        return productRepository.findAll();
    }

    public void deleteProduct(final Long id) {
    	productRepository.deleteById(id);
    }

    public Product saveProduct(Product product) {
    	Product savedProduct = productRepository.save(product);
        return savedProduct;
    }

}