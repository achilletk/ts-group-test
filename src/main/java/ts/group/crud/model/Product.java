package ts.group.crud.model;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "products")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "label")
	private String label;

	@Column(name = "description")
	private String description;

	@Column(name = "cost")
	private float cost;
	
	@Column(name = "state")
	private String state;
	
	public Product() {

	}

	public Product(String label, String description, String state) {
		this.label = label;
		this.description = description;
		this.state = state;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
